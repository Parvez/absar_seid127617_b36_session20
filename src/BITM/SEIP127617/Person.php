<?php
namespace App;
class Person
{
    public $name = "Absar";
    public $gender = "male";
    public $blood_group = "AB+";
    public $mobile = "01670357215";

    public function showPersonInfo()
    {
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
        echo $this->mobile."<br>";
    }

}